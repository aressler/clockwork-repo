﻿using System.Web.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using System;
using System.Configuration;

namespace Clockwork.Web.Controllers
{
    public class TimesController : Controller
    {
        static readonly string BackendWebOrigin = ConfigurationManager.AppSettings["backendWebOrigin"];
        static readonly string TimesAPI = BackendWebOrigin + "/api/times";
        static readonly string CurrentTimesAPI = BackendWebOrigin + "/api/times/current";

        /// <summary>
        /// Calls the backend API to get the current time.
        /// </summary>
        /// <returns></returns>
        private static async Task<HttpResponseMessage> _GetCurrentTime(string timezone)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(CurrentTimesAPI + $"?timezone={timezone}");
            client.Dispose();
            return response;
        }


        /// <summary>
        /// Calls the backend API to get a list of times.
        /// </summary>
        /// <returns></returns>
        private static async Task<HttpResponseMessage> _GetTimes(int page, int limit)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(TimesAPI + $"?page={page}&limit={limit}");
            client.Dispose();
            return response;
        }


        // This method is replaced during unit testing
        public Func<string, Task<HttpResponseMessage>> GetCurrentTime =
            async (string timezone) => await _GetCurrentTime(timezone);


        // This method is replaced during unit testing
        public Func<int, int, Task<HttpResponseMessage>> GetTimes =
            async (int page, int limit) => await _GetTimes(page, limit);


        /// <summary>
        /// Return the current time for the given timezone.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Current(string timezone)
        {
            Response.AddHeader("Content-Type", "application/json");
            HttpResponseMessage response = await GetCurrentTime(timezone);
            if (response.IsSuccessStatusCode) {
                var result = (await response.Content.ReadAsStringAsync());
                return Content(result);
            } else {
                // TODO: return proper exceptions
                return Content("{\"error\": \"Failed to get time.\"}");
            }
        }


        /// <summary>
        /// Return a list of times from the database.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index(int page=1, int limit=0)
        {
            Response.AddHeader("Content-Type", "application/json");
            HttpResponseMessage response = await GetTimes(page, limit);
            if (response.IsSuccessStatusCode) {
                var result = (await response.Content.ReadAsStringAsync());
                return Content(result);
            } else {
                // TODO: return proper exceptions
                return Content("{\"error\": \"Failed to get list of times.\"}");
            }
        }


    }
}
