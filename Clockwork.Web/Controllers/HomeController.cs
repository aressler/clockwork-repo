﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Script.Serialization;
using Clockwork.Web.Models;

namespace Clockwork.Web.Controllers
{
    public class HomeController : Controller
    {

        static readonly string BackendWebOrigin = ConfigurationManager.AppSettings["backendWebOrigin"];
        static readonly string TimezonesAPI = BackendWebOrigin + "/api/times/zones";


        /// <summary>
        /// Calls the backend API to get the current time.
        /// </summary>
        /// <returns></returns>
        private static async Task<HttpResponseMessage> _GetTimezones()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(TimezonesAPI);
            client.Dispose();
            return response;
        }

        public Func<Task<HttpResponseMessage>> GetTimezones = async () => await _GetTimezones();


        /// <summary>
        /// Aggregate timezones and current time API calls for the view.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            HttpResponseMessage response;
            var timesController = DependencyResolver.Current.GetService<TimesController>();
            timesController.ControllerContext = new ControllerContext(Request.RequestContext, timesController);
            // We don't know the requestor's time zone, so assume it's the server TZ.
            var tzId = TimeZoneInfo.Local.Id;

            // Run tasks in parallel.
            var tzTask = GetTimezones();
            var currentTimeTask = timesController.GetCurrentTime(tzId);

            // Let them complete.
            await Task.WhenAll(tzTask, currentTimeTask);

            // Handle timezones
            response = tzTask.Result;
            if (response.IsSuccessStatusCode) {
                var result = (await response.Content.ReadAsStringAsync());
                ViewData["Timezones"] = new JavaScriptSerializer().Deserialize<List<Timezone>>(result);
            } else {
                ViewData["Timezones"] = new List<Timezone>();
            }

            // Handle current time
            response = currentTimeTask.Result;
            if (response.IsSuccessStatusCode) {
                var result = (await response.Content.ReadAsStringAsync());
                ViewData["CurrentTime"] = new JavaScriptSerializer().Deserialize<CurrentTimeQuery>(result).Time;
            } else {
                ViewData["CurrentTime"] = "";
            }

            return View();
        }
    }
}
