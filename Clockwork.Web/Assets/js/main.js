﻿(function () {
    'use strict';

    let clockDisplay, menu, clock, timeDrawer, timeTable, timezones;

    jQuery(main);



    function convertTimeString(time) {
        const date = new Date(time);
        return date.toLocaleTimeString();
    }

    async function updateClockDisplay() {
        const timezone = timezones.val();
        let result = await fetch(`/times/current?timezone=${timezone}`);

        result = await result.json();
        if (result) {
            clockDisplay.html(convertTimeString(result.time));
        }
    }

    function onMenuClick(event) {
        event.preventDefault();
        event.stopPropagation();
        clock.toggleClass('show-menu');
    }

    function onClockClick() {
        clock.removeClass('show-menu');
        updateClockDisplay();
    }

    // Setup and event bindings.
    function main() {
        // Setup.
        clock = jQuery('#clock');
        clockDisplay = clock.find('.display');
        menu = clock.find('.menu');
        timeDrawer = jQuery('.time-drawer');
        timeTable = jQuery('#time-table');
        timezones = jQuery('#timezones');

        // Bind.
        menu.on('click', onMenuClick);
        clock.on('click', onClockClick);
        menu.find('.list').on('click', event => {
            event.stopPropagation();
        });
        menu.find('.actions .time-drawer-toggle').on('click', () => timeDrawer.toggleClass('show'));
        timeDrawer.find('.close').on('click', () => timeDrawer.removeClass('show'));
        timeDrawer.find('.refresh').on('click', () => timeTable.DataTable().ajax.reload());

        // Setup the data table.
        timeTable.DataTable({
            serverSide: true,
            searching: false,
            ordering: false,
            stateSave: true,
            pageLength: 10,
            ajax: (data, callback, settings) => {
                const draw = data.draw;
                const page = 1 + Math.trunc(data.start / data.length);

                jQuery.ajax({
                    url: `/times?page=${page}&limit=${data.length}`,
                    success: (data, status, jQxhr) => {
                        callback({
                            draw: draw,
                            recordsTotal: data.meta.total,
                            recordsFiltered: data.meta.total,
                            data: data.items.map(item => [
                                item.currentTimeQueryId,
                                `<span class="time">${convertTimeString(item.time)}</span>`,
                                item.clientIp,
                                `<span class="time">${convertTimeString(item.utcTime)}</span>`,
                                item.timezoneId,
                            ]),
                        });
                    },
                    // TODO
                    error: console.error,
                });
            },
        });

        // The server's view inserted an unformated time in the clock.
        const time = clockDisplay.html();
        if (time) {
            clockDisplay.html(convertTimeString(time));
        }
        clock.removeClass('hidden');
    }
}());