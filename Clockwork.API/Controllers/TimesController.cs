﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Clockwork.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Clockwork.API.Controllers
{
    public class TimesController : Controller
    {

        private static List<Timezone> Zones = null;
        private static List<string> ZoneIds = null;


        /// <summary>
        /// This is the method to use when getting the list of supported timezones.
        /// </summary>
        /// <returns></returns>
        public List<Timezone> GetTimezones ()
        {
            if (Zones == null) {
                Zones = new List<Timezone>();
                ZoneIds = new List<string>();
                ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();
                foreach (var item in zones)
                {
                    Zones.Add(new Timezone() { Id = item.Id, DisplayName = item.DisplayName });
                    ZoneIds.Add(item.Id);
                }
            }
            return Zones;
        }


        // GET: /api/times
        /// <summary>
        /// This action returns a list of CurrentTimeQueries from the database.
        /// </summary>
        /// <param name="page">1-indexed page offset. Default 1.</param>
        /// <param name="limit">The maximum number of items to return. Default 20; max 500.</param>
        /// <returns>A list of database time results.</returns>
        [HttpGet]
        [Route("api/times")]
        public IActionResult Index(int page=1, int limit=20)
        {
            using (var db = new ClockworkContext())
            {
                // Pages are 1-indexed
                page = Math.Max(1, page) - 1;
                limit = Math.Max(1, Math.Min(limit, 500));

                var results = db.CurrentTimeQueries
                                .OrderByDescending(x => x.CurrentTimeQueryId)
                                .Skip(page * limit)
                                .Take(limit)
                                .ToList();
                var model = new TimesResponse() { Items = results, Meta = { total = db.CurrentTimeQueries.Count() } };
                return Ok(model);
            }
        }


        // GET /api/times/current
        /// <summary>
        /// Return the current time.
        /// Save this request to the database.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/times/current")]
        public IActionResult Current(string timezone = null)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = HttpContext.Connection.RemoteIpAddress.ToString();
            var tzId = timezone ?? TimeZoneInfo.Local.Id;

            // If this request was for a timezone that didn't exist, then default to local.
            // Ideally this would never happen because this API provides the supported list of TZs.
            GetTimezones();
            if (!ZoneIds.Contains(tzId)) {
                tzId = TimeZoneInfo.Local.Id;
            } 

            var tzInfo = TimeZoneInfo.FindSystemTimeZoneById(tzId);
            var returnVal = new CurrentTimeQuery {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = TimeZoneInfo.ConvertTime(serverTime, tzInfo),
                TimezoneId = tzId,
            };

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
            }

            return Ok(returnVal);
        }


        /// <summary>
        /// Returns the list of time zones supported by this system.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/times/zones")]
        public IActionResult Timezones() {
            return Ok(GetTimezones());
        }


    }
}
