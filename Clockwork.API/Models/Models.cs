﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Models
{
    public class ClockworkContext : DbContext
    {
        public DbSet<CurrentTimeQuery> CurrentTimeQueries { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=clockwork.db");
        }
    }

    public class CurrentTimeQuery
    {
        public int CurrentTimeQueryId { get; set; }
        public DateTime Time { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
        public string TimezoneId { get; set; }
    }


    public class TimesResponse
    {
        public class Info
        {
            public int total;
        }
        public List<CurrentTimeQuery> Items { get; set; }
        public Info Meta { get; set; }

        public TimesResponse()
        {
            Meta = new Info();
        }
    }


    public class Timezone
    {
        public string Id;
        public string DisplayName;
    }

}
