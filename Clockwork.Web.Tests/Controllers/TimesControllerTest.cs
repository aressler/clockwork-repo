﻿using System.Web;
using NUnit.Framework;
using System.Threading.Tasks;
using Clockwork.Web.Controllers;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.IO;
using System.Net;

namespace Clockwork.Web.Tests.Controllers
{
    [TestFixture]
    class TimesControllerTest
    {
        static readonly string TimeContent = "{\"time\": 1111111111}";
        static readonly string TimesContent = "{\"meta\": {\"total\": 1}, \"items\": [{\"currentQueryId\": 1, \"utcTime\": 1, \"time\": 1, \"timezone\": \"Pacific Standard Time\", \"clientIp\": \"127.0.0.1\"}]";

        static readonly string Timezone = "Pacific Standard Time";

        // TODO: It might be better to have the controller implement an interface
        //       that is mockable rather than replacing a delegate handler.
        public async Task<HttpResponseMessage> MockGetCurrentTime(string timezone)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            responseMessage.Content = new StringContent(
                TimeContent, System.Text.Encoding.UTF8, "application/json");
            await Task.Delay(1);
            return responseMessage;
        }


        public async Task<HttpResponseMessage> MockGetTimes(int page, int limit)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            responseMessage.Content = new StringContent(
                TimesContent, System.Text.Encoding.UTF8, "application/json");
            await Task.Delay(1);
            return responseMessage;
        }

        [Test]
        public async Task Current()
        {
            // Arrange
            // Not sure I like having to do all this setup to mock a request.
            // A helper may be the way to go, or maybe there's a better way.
            // Reference: https://stackoverflow.com/questions/9871700/mvc3-unit-testing-response-code/9871881#9871881
            var request = new HttpRequest("", "http://127.0.0.1/times/current", "");
            var response = new HttpResponse(TextWriter.Null);
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            var controller = new TimesController();
            var ctx = new ControllerContext(
                httpContext, new RouteData(), controller);
            // All that setup to create the controller and set a context.
            controller.ControllerContext = ctx;
            // In reality, the GetCurrentTime call makes an API call.
            controller.GetCurrentTime = MockGetCurrentTime;

            // Act
            var result = (ContentResult)await controller.Current(Timezone);

            // Assert
            Assert.AreEqual(TimeContent, result.Content);
        }

        [Test]
        public async Task Index()
        {
            int page = 1,
                limit = 10;
            var request = new HttpRequest("", $"http://127.0.0.1/times?page={page}&limit={limit}", "");
            var response = new HttpResponse(TextWriter.Null);
            var httpContext = new HttpContextWrapper(new HttpContext(request, response));
            var controller = new TimesController();
            var ctx = new ControllerContext(
                httpContext, new RouteData(), controller);
            // All that setup to create the controller and set a context.
            controller.ControllerContext = ctx;
            // In reality, the GetCurrentTime call makes an API call.
            controller.GetTimes = MockGetTimes;

            // Act
            var result = (ContentResult)await controller.Index(page, limit);

            // Assert
            Assert.AreEqual(TimesContent, result.Content);
        }
    }
}
